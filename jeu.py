#!/bin/python
import pygame
import os
from graphalama.core import WidgetList
from graphalama.widgets import Button, SimpleText
from graphalama.constants import CENTER, RAINBOW, BOTTOMRIGHT, LLAMA, NICE_BLUE, TOPLEFT
from graphalama.colors import MultiGradient, Gradient
from graphalama.shapes import RoundedRect, Padding
from params import Params
from player import Player
from coins import Coins

MODE_QUIT = -1
MODE_MENU = 0
MODE_GAME = 1
MODE_SETTINGS = 2

FPS = 60

pygame.init()


def draw_background(display):
    img = pygame.image.load("C:/Users/annec/Desktop/le-jeu/assets/backgrounds/background_rainbow.jpg")
    size = pygame.display.get_surface().get_size()
    background = pygame.transform.scale(img, size)
    display.blit(background, (0, 0))

def main_menu(display, clock):
    main_menu.mode = MODE_MENU

    def go_play():
        main_menu.mode = MODE_GAME
    def go_settings():
        main_menu.mode = MODE_SETTINGS
    def go_quit():
        main_menu.mode = MODE_QUIT

    widgets = WidgetList([
        Button(text="Play",
                function=go_play,
                pos=(100, 100),
                anchor=TOPLEFT),
        Button(text="settings",
               function=go_settings,
               pos=(100, 300),
               anchor=TOPLEFT),
         Button(text="quit",
                function = go_quit,
                pos=(100, 500),
                anchor=TOPLEFT)
    ])

    while main_menu.mode == MODE_MENU:
        # Processing events
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                go_quit()
            widgets.update(event)
        draw_background(display)
        widgets.render(display)
        clock.tick(FPS)
        pygame.display.flip()

    return main_menu.mode

def main_game(display, clock):
    pass

def main_settings(display, clock):
    pass

def main():
    mode = MODE_MENU
    display = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    clock = pygame.time.Clock()
    
    while mode != MODE_QUIT:
        if mode == MODE_MENU:
            mode = main_menu(display, clock)
        elif mode == MODE_GAME:
            mode = main_game(display, clock)
        elif mode == MODE_SETTINGS:
            mode = main_settings(display, clock)

if __name__ == "__main__":
    main()
    print("bye bye")
