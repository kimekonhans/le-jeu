import pygame
from graphalama.core import Widget
from graphalama.colors import ImageBrush
from graphalama.constants import STRETCH
from graphalama.maths import Pos
from graphalama.shadow import NoShadow
from params import Params
from math import sqrt


class Player(Widget):

    def __init__(self, pos, path):
        pass

    def pre_render_update(self):
        pass

    def on_key_press(self, event):
        pass

    def on_key_release(self, event):
        pass

    def catch_coins(self, coins):
        pass
